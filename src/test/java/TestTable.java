/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.titima.oxprogramoop.Player;
import com.titima.oxprogramoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class TestTable {

    public TestTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testRow2ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testRow3ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testcol1ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testcol2ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testcol3ByX() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testRow1ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testRow2ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testRow3ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testcol1ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testcol2ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testcol3ByO() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testRow1ByX1() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWiner());

    }

    @Test
    public void testRow1Byo1() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWiner());

    }

    @Test
    public void testRowColIsNotEmpty() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        assertEquals(false, table.setRowCol(0, 0));

    }

    @Test
    public void testDraw() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkDraw();
        assertEquals(true, table.isFinish());

    }
}
