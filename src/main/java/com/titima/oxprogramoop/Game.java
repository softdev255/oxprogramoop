package com.titima.oxprogramoop;

import java.util.Scanner;

public class Game {

    private Player playerX;
    private Player playerO;
    private Player playerTurn;
    public Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    public void showTable() {
        table.showTable();
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWiner() == null) {
                    System.out.println("Draw!!!");
                } else {
                    System.out.println(table.getWiner().getName() + " Win!!!");
                }
                this.showTable();
                System.out.println("You want to play another game");
                System.out.println("Yes / No");
                String ans = kb.next();
                if (ans.equals("Yes")) {
                    newGame();
                } else {
                    System.out.println("Bye Bye");
                    break;
                }

            }
            table.switchPlayer();
        }
    }
}
